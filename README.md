# This is a basic api project

## local installation
Duplicate & configure your .env file
```console
cp .env.dist .env
```

Build Docker
```bash
docker-compose build
```

Start Docker
```bash
docker-compose up
```


