<?php

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

/**
 * Class ValidatorException
 */
class EntityValidatorException extends \Exception
{
    /**
     * @var ConstraintViolationListInterface
     */
    private ConstraintViolationListInterface $constraintViolationList;

    /**
     * EntityValidatorException constructor.
     *
     * @param ConstraintViolationListInterface $constraintViolationList
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        ConstraintViolationListInterface $constraintViolationList,
        $code = 0,
        Throwable $previous = null
    ) {
        $this->constraintViolationList = $constraintViolationList;

        parent::__construct($constraintViolationList->__toString(), $code, $previous);
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getConstraintViolationList(): ConstraintViolationListInterface
    {
        return $this->constraintViolationList;
    }
}
