<?php

namespace App\Services\Entity;

use App\Exception\EntityValidatorException;
use App\Managers\AbstractEntityManager;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractEntityService
 */
abstract class AbstractEntityService
{
    /**
     * @var AbstractEntityManager
     */
    protected $manager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->manager->getEntityClass();
    }

    /**
     * @required
     *
     * @param ValidatorInterface $validator
     *
     * @return self
     */
    public function setValidator(ValidatorInterface $validator): self
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * Use the manager by extending the AbstractEntityManager class
     *
     * AbstractService constructor.
     *
     * @param AbstractEntityManager $manager
     */
    public function __construct(AbstractEntityManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param string $id
     *
     * @return object|null
     */
    public function find(string $id)
    {
        $object = $this->manager->find($id);
        if (!$object) {
            return null;
        }
        return $object;
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int        $limit
     * @param null       $offset
     *
     * @return array
     */
    public function getBy(array $criteria, ?array $orderBy = null, $limit = 50, $offset = null): array
    {
        return $this->manager->getBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * Finds multiple entities by a set of criteria passed to the manager for custom queries
     *
     * @param array $array
     * @param bool $groupBy
     *
     * @return array
     */
    public function getByProperties(array $array, bool $groupBy = false): array
    {
        return $this->manager->getByProperties($array, $groupBy);
    }

    /**
     * Finds a single entity by a set of criteria.
     *
     * @param mixed[] $criteria The criteria.
     *
     * @return object|null The entity.
     */
    public function findBy(array $criteria)
    {
        return $this->manager->findBy($criteria);
    }

    /**
     * @param $entity
     */
    public function refresh($entity): void
    {
        $this->manager->refresh($entity);
    }

    /**
     * @param $entity
     *
     * @param bool $flush
     * @return object
     *
     * @throws EntityValidatorException
     */
    public function create($entity, $flush = true)
    {
        $this->validate($entity);

        $this->manager->create($entity, $flush);

        return $entity;
    }

    /**
     * @param $entity
     *
     * @param bool $flush
     * @return mixed
     *
     * @throws EntityValidatorException
     */
    public function update($entity, $flush = true)
    {
        $this->validate($entity);

        $this->manager->update($entity, $flush);

        return $entity;
    }

    /**
     * @param $entity
     * @param bool $flush
     *
     * @return void
     */
    public function delete($entity, $flush = true): void
    {
        $this->manager->delete($entity, $flush);
    }

    /**
     * @param $entity
     *
     * @throws EntityValidatorException
     */
    public function validate($entity): void
    {
        $validation = $this->validator->validate($entity);

        if ($validation->count() !== 0) {
            throw new EntityValidatorException($validation);
        }
    }
}
