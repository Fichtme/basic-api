<?php

namespace App\Services\Entity;

use App\Entity\Blog;
use App\Managers\BlogManager;

/**
 * Class BlogService
 * @package App\Services\Entity
 */
class BlogService extends AbstractEntityService
{
    /**
     * BlogService constructor.
     * @param BlogManager $manager
     */
    public function __construct(BlogManager $manager)
    {
        parent::__construct($manager);
    }

    /**
     * @param string $id
     * @return object
     */
    public function find(string $id)
    {
        $blog = parent::find($id);
        if (!$blog) {
            return null;
        }

        $blog->addView();
        $this->update($blog, true);

        return $blog;
    }

    /**
     * @param array $array
     * @param bool $groupBy
     *
     * @return Blog[]|array
     */
    public function getByProperties(array $array, bool $groupBy = false): array
    {
        /** @var Blog[] $blogs */
        $blogs = parent::getByProperties($array, $groupBy);

        $this->incrementViewCount($blogs);

        return $blogs;
    }

    /**
     * @param array $blogs
     */
    private function incrementViewCount(array $blogs): void
    {
        foreach ($blogs as $blog) {
            $blog->addView();
        }
        $this->manager->flush();
    }
}
