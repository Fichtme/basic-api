<?php
namespace App\Managers;

use App\Entity\Blog;

/**
 * Class BlogManager
 */
class BlogManager extends AbstractEntityManager
{
    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return Blog::class;
    }

    /**
     * @param array $properties
     * @param bool $groupBy
     *
     * @return Blog[]
     */
    public function getByProperties(array $properties = [], bool $groupBy = false): array
    {
        $qb = $this->createQueryBuilder();

        return $qb->getQuery()->getResult();
    }
}
