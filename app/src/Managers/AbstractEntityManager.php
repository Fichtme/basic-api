<?php

namespace App\Managers;

use App\Exception\NotImplementedException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;

/**
 * Dont call functions by extending the manager directly, use an EntityService between it to ensure business logic
 *
 * Class AbstractEntityManager
 *
 * @package Middleware\Managers
 */
abstract class AbstractEntityManager
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @required
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return self
     */
    public function setManager(EntityManagerInterface $entityManager): self
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select('o')
            ->from($this->getRepository()->getClassName(), 'o');


        # TODO: automatically adjust limit / page / from / to

        $qb->setMaxResults(20);

        # TODO: implement delete listener interface, check for interface and modify query to remove soft deletions

        return $qb;
    }

    /**
     * Gets the repository for the entity class.
     *
     * @return ObjectRepository
     */
    protected function getRepository(): ObjectRepository
    {
        return $this->entityManager->getRepository($this->getEntityClass());
    }

    /**
     * The class name of the entity belonging to this manager
     *
     * @return string
     */
    abstract public function getEntityClass(): string;

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param string $id
     *
     * @return object|null
     */
    public function find(string $id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * Finds a single entity by a set of criteria.
     *
     * @param mixed[] $criteria The criteria.
     *
     * @return object|null The entity.
     */
    public function findBy(array $criteria)
    {
        return $this->getRepository()->findOneBy($criteria);
    }

    /**
     * Note: Please refrain from using this function!
     *
     * @return array
     */
    public function getAll(): array
    {
        return $this->getRepository()->findAll();
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     *
     * @return array
     */
    public function getBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->getRepository()
            ->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param array $properties
     * @param bool $groupBy
     *
     * @return array
     *
     * @throws NotImplementedException
     */
    public function getByProperties(array $properties = [], bool $groupBy = false): array
    {
        throw new NotImplementedException(sprintf('%s is not implemented', __METHOD__));
    }

    /**
     * The object will be entered into the database as a result of the flush operation.
     *
     * @param $entity
     * @param bool $flush
     *
     * @return object
     */
    public function create($entity, bool $flush = true)
    {
        $this->entityManager->persist($entity);

        if ($flush) {
            $this->entityManager->flush();
        }

        return $entity;
    }

    /**
     * A convenient method to tell the manager to flush entity changes to the database
     *
     * @param $entity
     * @param bool $flush
     *
     * @return object
     */
    public function update($entity, bool $flush = true)
    {
        if ($flush) {
            $this->entityManager->flush();
        }

        return $entity;
    }

    /**
     * Removes an object instance.
     *
     * A removed object will be removed from the database as a result of the flush operation.
     *
     * @param object $entity The object instance to remove.
     * @param bool $flush
     * @return void
     */
    public function delete($entity, bool $flush): void
    {
        $this->entityManager->remove($entity);

        if ($flush) {
            $this->flush();
        }
    }

    /**
     * Flushes all changes to objects that have been queued up to now to the database.
     * This effectively synchronizes the in-memory state of managed objects with the
     * database.
     *
     * @return void
     */
    public function flush(): void
    {
        $this->entityManager->flush();
    }

    /**
     * Refreshes the persistent state of an object from the database,
     * overriding any local changes that have not yet been persisted.
     *
     * @param object $entity The object to refresh.
     *
     * @return void
     */
    public function refresh($entity): void
    {
        $this->entityManager->refresh($entity);
    }

    /**
     * Detaches all objects for this class from the ObjectManager, causing managed objects to
     * become detached. Not flushed changes made to the objects if any
     * (including removal of a object), will not be synchronized to the database.
     * Objects which previously referenced the detached object will continue to
     * reference it.
     */
    private function clear(): void
    {
        $this->entityManager->clear($this->getEntityClass());
    }
}
