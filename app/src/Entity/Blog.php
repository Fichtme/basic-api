<?php

namespace App\Entity;

use App\Repository\BlogRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass=BlogRepository::class)
 */
class Blog
{
    /**
     * @JMS\Groups({"id"})
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private ?int $id;

    /**
     * @JMS\Groups({"blog", "blogs"})
     *
     * @ORM\Column(type="string", length=255)
     */
    private ?string $title;

    /**
     * @JMS\Groups({"blog", "blogs"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $article;

    /**
     * @JMS\Groups({"blog", "blogs"})
     *
     * @ORM\Column(type="integer")
     */
    private int $views;

    /**
     * @ORM\OneToMany(targetEntity=BlogComment::class, mappedBy="blog", orphanRemoval=true)
     */
    private $blogComments;

    /**
     * Blog constructor.
     */
    public function __construct()
    {
        $this->views = 0;
        $this->blogComments = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getArticle(): ?string
    {
        return $this->article;
    }

    /**
     * @param string|null $article
     * @return $this
     */
    public function setArticle(?string $article): self
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getViews(): ?int
    {
        return $this->views;
    }

    /**
     * @param int $views
     * @return $this
     */
    public function setViews(int $views): self
    {
        $this->views = $views;

        return $this;
    }

    /**
     * @return $this
     */
    public function addView(): self
    {
        ++$this->views;
        return $this;
    }

    /**
     * @return Collection|BlogComment[]
     */
    public function getBlogComments(): Collection
    {
        return $this->blogComments;
    }

    /**
     * @param BlogComment $blogComment
     * @return $this
     */
    public function addBlogComment(BlogComment $blogComment): self
    {
        if (!$this->blogComments->contains($blogComment)) {
            $this->blogComments[] = $blogComment;
            $blogComment->setBlog($this);
        }

        return $this;
    }

    /**
     * @param BlogComment $blogComment
     * @return $this
     */
    public function removeBlogComment(BlogComment $blogComment): self
    {
        if ($this->blogComments->contains($blogComment)) {
            $this->blogComments->removeElement($blogComment);
            // set the owning side to null (unless already changed)
            if ($blogComment->getBlog() === $this) {
                $blogComment->setBlog(null);
            }
        }

        return $this;
    }
}
