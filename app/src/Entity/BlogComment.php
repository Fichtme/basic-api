<?php

namespace App\Entity;

use App\Repository\BlogCommentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BlogCommentRepository::class)
 */
class BlogComment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Blog::class, inversedBy="blogComments")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Blog $blog;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $comment;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return BlogComment
     */
    public function setId(?int $id): BlogComment
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Blog|null
     */
    public function getBlog(): ?Blog
    {
        return $this->blog;
    }

    /**
     * @param Blog|null $blog
     * @return BlogComment
     */
    public function setBlog(?Blog $blog): BlogComment
    {
        $this->blog = $blog;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     * @return BlogComment
     */
    public function setComment(?string $comment): BlogComment
    {
        $this->comment = $comment;
        return $this;
    }
}
