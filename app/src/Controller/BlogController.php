<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Services\Entity\BlogService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;

/**
 * Class BlogController
 * @Rest\View(serializerEnableMaxDepthChecks=true, serializerGroups={"id", "blogs"})
 * @package App\Controller
 */
class BlogController extends AbstractFOSRestController
{
    private BlogService $blogService;

    /**
     * BlogController constructor.
     * @param BlogService $blogService
     */
    public function __construct(BlogService $blogService)
    {
        $this->blogService = $blogService;
    }

    /**
     * @param string $id
     * @return Blog|null
     */
    public function getAction(string $id): ?Blog
    {
        return $this->blogService->find($id);
    }

    /**
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     * @param ParamFetcher $param
     * @return Blog[]
     */
    public function listAction(ParamFetcher $param): array
    {
        return $this->blogService->getByProperties($param->all());
    }
}
